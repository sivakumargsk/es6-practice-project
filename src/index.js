// Implement reduce, map, filter as curried functions.
// Use generators added in ES2015. DO NOT use arrays.
//  Use for or while loops, do not use any library.

function* generateColl(size) {
  let item = 1;
  while (item <= size) {
    yield item;
    item += 1;
  }
}

function* generateRandomColl(size) {
  let item = 1;
  while (item <= size) {
    yield Math.floor(Math.random() * size) + 1;
    item += 1;
  }
}

// --------------------------------------------
const map = fun =>
  function* mapGenerator(coll) {
    for (const item of coll) yield fun(item);
  };

const square = num => num * num;

console.log('map function for square 1 - 10');
console.log([...map(square)(generateColl(10))]);

// ---------------------------------------------
function* reduceGenerator(fun, initialVal, coll) {
  let result = initialVal;
  for (const item of coll) result = fun(result, item);
  yield result;
}

const reduce = fun => (...args) => {
  if (args.length === 2) {
    const [first, rest] = args;
    return reduceGenerator(fun, first, rest);
  }
  const [[first, ...rest]] = args;
  return reduceGenerator(fun, first, rest);
};

// const reduce = fun => (initialVal, coll) => {
//   return function* reduceGenerator(coll) {
//     let result = initialVal;
//     for (const item of coll) result = fun(result, item);
//     return result;
//   };
// };

const addTwo = (num1, num2) => num1 + num2;

console.log('Reduce Function for add 1 - 10');
console.log([...reduce(addTwo)(generateColl(10))]);

// ------------------------------------------------
const filter = fun =>
  function* filterGenerator(coll) {
    for (const item of coll) if (fun(item)) yield item;
  };

const isEven = num => num % 2 === 0;
console.log('Filter Function for even 1 - 20');
console.log([...filter(isEven)(generateColl(20))]);

// Implement unique, max, toArray using reduce function.
// DO NOT use for loops.

// ----------------------------------------------------
const addValueIfNot = (coll, value) =>
  coll.includes(value) ? coll : [...coll, value];

const unique = coll => reduce(addValueIfNot)([], coll).next().value;

console.log('unique function for random numbers');
console.log(unique(generateRandomColl(20)));

// -----------------------------------------------------
const maxTwo = (num1, num2) => (num1 > num2 ? num1 : num2);
const max = coll => reduce(maxTwo)(coll).next().value;

console.log('max function for random numbers');
console.log(max(generateRandomColl(20)));

// -------------------------------------------------------
const addToColl = (coll, value) => [...coll, value];
const toArray = coll => reduce(addToColl)([], coll).next().value;

console.log('toArray function for 1 - 20');
console.log(toArray(generateColl(20)));

// Implement compose function which can take any number of
// parameters. Also implement pipeline which works like
// clojure’s ->> operator( similar to lodash chain, ramda pipeline )

const compose = (...funs) =>
  funs.reduce((fun1, fun2) => (...args) => fun1(fun2(...args)));

const addition = (...args) => reduce(addTwo)(args).next().value;

const addAndSqure = compose(square, addition)(1, 2, 3, 4);
console.log(addAndSqure);

const pipeLine = (...funs) =>
  funs.reduce((fun1, fun2) => (...args) => fun2(fun1(...args)));

const squreAndAdd = pipeLine(addition, square)(1, 2, 3);
console.log(squreAndAdd);
